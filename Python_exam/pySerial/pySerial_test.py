# coding=UTF-8
import sys
import serial
import time


# --------------------------------
def InitSerialPort():
    ser = serial.Serial(
    port='/dev/ttyUSB0',
	# port='COM22',
    baudrate=115200,
    parity=serial.PARITY_NONE,
    stopbits=serial.STOPBITS_ONE,
    bytesize=serial.EIGHTBITS,
    timeout=1
   )
    return ser

# --------------------------------
def SerialACK():
    AckStr = Serial.readline()
    AckHexStr = AckStr.encode('hex')
    if (AckHexStr.strip()):
        print 'Ack Raw Data =',AckHexStr
        if (AckHexStr[8:10] == '00'):
            return True
        else:
            return False

# --------------------------------
if __name__ == "__main__":

    reload(sys)
    sys.setdefaultencoding('utf-8')


    Serial = InitSerialPort()

    #factory ON
    # Cmd = "\xC5\x63\x07\x02\x00\x01\xA2"
    #factory Off
    Cmd = "\xC5\x63\x07\x02\x00\x00\xA3"
    Serial.write(Cmd)
    time.sleep(0.5)

    if(SerialACK() == True):
        print ("Success")
    else:
        print ("Fail")