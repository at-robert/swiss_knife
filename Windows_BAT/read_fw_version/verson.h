#ifndef __AMT_VER_INFO_H__
#define __AMT_VER_INFO_H__

/* VIZIO Sound bar version information. */

#define AMT_VER_INFO_REVISION "01254"    /* SVN revision, max: 5 bytes. */

#define AMT_VER_INFO_VERSION_VIZIO_SB44XX_C0  "0906"     /* SB44XX-C0 fw version, max: 4 bytes. */

#define AMT_VER_INFO_VERSION_VIZIO_SB38XX_D0  "0906"     /* SB38XX-D0 fw version, max: 4 bytes. */

#endif // __AMT_VER_INFO_H__
