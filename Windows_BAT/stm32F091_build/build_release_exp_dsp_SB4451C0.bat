@echo off 
set IAR_BIN_PATH="C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.0\common\bin"
set SOURCE_CODE_PATH="D:\SourceCode\stm32F091_ext_dsp"
set DESTINATION_PATH="X:\Test\4451"
set PROJECT_NAME=SB4451
set PROJECT_NAME1=SB4451_C0

set CLEAN_FIRST=1
set BUILD_LOADER=1
set BUILD_APP=1
set COPY_RESULT=1

:: start build app
if %BUILD_APP%==1 (
	:: delete output file first
	if exist %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Application\%PROJECT_NAME1%.bin (
		del %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Application\%PROJECT_NAME1%.bin
	)

	echo "**************************"
	echo "Build %PROJECT_NAME1% App"
	echo "**************************"
	if %CLEAN_FIRST%==1 (
		%IAR_BIN_PATH%\iarbuild.exe %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\%PROJECT_NAME1%\%PROJECT_NAME1%_app.ewp -clean app -log info
	)
	%IAR_BIN_PATH%\iarbuild.exe %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\%PROJECT_NAME1%\%PROJECT_NAME1%_app.ewp -make app -log info

	:: pause if build fail
	if not exist %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Application\%PROJECT_NAME1%.bin (
		echo.
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "Build %PROJECT_NAME1% App Fail"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		pause
		set BUILD_LOADER=0
	)
)

:: start build loader
if %BUILD_LOADER%==1 (
	:: delete output file first
	if exist %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Release\%PROJECT_NAME1%_SMT.bin (
		del %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Release\%PROJECT_NAME1%_SMT.bin
	)

	echo "**************************"
	echo "Build %PROJECT_NAME1% Loader"
	echo "**************************"
	if %CLEAN_FIRST%==1 (
		%IAR_BIN_PATH%\iarbuild.exe %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\%PROJECT_NAME1%\%PROJECT_NAME1%_loader.ewp -clean Release -log info
	)
	%IAR_BIN_PATH%\iarbuild.exe %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\%PROJECT_NAME1%\%PROJECT_NAME1%_loader.ewp -make Release -log info

	:: pause if build fail
	if not exist %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Release\%PROJECT_NAME1%_SMT.bin (
		echo.
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "Build %PROJECT_NAME1% Loader Fail"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		pause
	)
)

::copy the final into a certain folder
if %COPY_RESULT%==1 (
		echo.
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		echo "Build %PROJECT_NAME1% copy the result files"
		echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!"
		
		copy %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Application\%PROJECT_NAME1%.bin %DESTINATION_PATH%\%PROJECT_NAME1%.bin
		copy %SOURCE_CODE_PATH%\Projects\SB4451\EWARM\Projects\Output\%PROJECT_NAME1%\Release\%PROJECT_NAME1%_SMT.bin %DESTINATION_PATH%\%PROJECT_NAME1%_SMT.bin
		pause
)