ECHO OFF

SET VERSION=%1

SET MODEL=S3251A
SET sMODEL=s3251a


SET VER_FILE=D:\SourceCode\VizioSB_2017_3521_2820\Projects\App_SRC\Include\Defs.h
ECHO /********** Reading SB3251 Version **********/
for /f "tokens=3" %%a in ('findstr main_version_3251 %VER_FILE%') do ( set main_ver=%%a ) 
set _result=%main_ver:0x=%
set _result=%_result: =%
SET VERSION=%_result%
echo SB3251 Version = %VERSION%

ECHO /********** Reading SB3251 SVN **********/
for /f "tokens=3" %%a in ('findstr svn_version_3251 %VER_FILE%') do ( set svn_ver=%%a )
echo SB3251 SVN Version = %svn_ver% 

set IAR_BIN_PATH="C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.0\common\bin\iarbuild.exe"
SET OUT_FILE=%MODEL%.bin
SET SMT_FILE=%MODEL%_SMT.bin
SET STM32_FILE=stm32.bin
SET DSP_FILE=DSPuld.bin
SET DSP_CHECK_SUM_FILE=DSPchecksum.bin
SET CCK_FILE=NoiseSound.bin
SET CCK_VERSION_FILE=NoiseVersion.bin
SET DSP_IMAGE_PATH=..\cs495314_dsp_bin\%MODEL%
SET CCK_IMAGE_PATH=..\voice_checking_bin
SET APP_PATH=..\Output\%MODEL%
SET SMT_PATH=..\Output\%MODEL%\Release
SET REL_PATH=..\Output\%MODEL%_V%VERSION%_RELEASE
SET ONEBIN_PATH=OutputBinFile

SET OUT_FILE=%MODEL%.bin
SET OUT_SMT_FILE=%MODEL%_SMT.bin

SET OUT_FLASH_V_FILE=%MODEL%_V%VERSION%_SPI_FLASH.bin
SET OUT_SMT_V_FILE=%MODEL%_V%VERSION%_SMT.bin

SET BT_AB1520_FILE=AB1520.bin
SET BT_AB1520_PATH=..\ab1520_bin

SET WINRAR_EXE="C:\Program Files\WinRAR\Rar.exe"
SET WINRAR_FILE=X:\stm32f105\FW\VIZIO_SB_2017\SB3251\S3251A_V%VERSION%_REV%svn_ver%_WEEKLY_RELEASE.rar

SET OUT_REL_FOLDER=X:\stm32f105\FW\VIZIO_SB_2017\SB3251\S3251A_V%VERSION%_RELEASE
SET OUT_GSYNC_FOLDER=X:\GSync\Python\stm32_merge_bin

ECHO ==================== S T A R T   B U I L D I N G ===============================

rmdir /S ..\OUTPUT

ECHO ******************** B U I L D   %MODEL% A P P L I C A T I O N **************
%IAR_BIN_PATH% %MODEL%\%sMODEL%_app.ewp -clean APP_%MODEL% -log info
%IAR_BIN_PATH% %MODEL%\%sMODEL%_app.ewp -make APP_%MODEL% -log info

ECHO ******************** B U I L D   %MODEL%  R E L E A S E ********************
%IAR_BIN_PATH% %MODEL%\%sMODEL%_loader.ewp -clean %MODEL%_RELEASE -log info
%IAR_BIN_PATH% %MODEL%\%sMODEL%_loader.ewp -make %MODEL%_RELEASE -log info


ECHO ******************** C O P Y   B I N A R A Y ***********************************
rmdir /s %REL_PATH%
mkdir %REL_PATH%  
copy %APP_PATH%\%OUT_FILE% %REL_PATH%\%STM32_FILE%
copy %SMT_PATH%\%SMT_FILE% %REL_PATH%\
copy %CCK_IMAGE_PATH%\*.bin %REL_PATH%\
copy %DSP_IMAGE_PATH%\*.bin %REL_PATH%\ 

ECHO ******************** C O N B I N E  F I L E ************************************

rmdir %REL_PATH%\%ONEBIN_PATH%
mkdir %REL_PATH%\%ONEBIN_PATH%

rmdir %BT_AB1520_PATH%\%ONEBIN_PATH%

copy /b %REL_PATH%\stm32.bin + %REL_PATH%\%CCK_VERSION_FILE% + %REL_PATH%\%DSP_CHECK_SUM_FILE% + %REL_PATH%\NoiseSound.bin + %REL_PATH%\flash_image_dsp.bin %REL_PATH%\%OUT_FILE%
copy /b %REL_PATH%\%OUT_SMT_FILE% %REL_PATH%\%ONEBIN_PATH%\%OUT_SMT_V_FILE%
copy /b %REL_PATH%\%CCK_VERSION_FILE% + %REL_PATH%\%DSP_CHECK_SUM_FILE% + %REL_PATH%\NoiseSound.bin + %REL_PATH%\flash_image_dsp.bin %REL_PATH%\%ONEBIN_PATH%\%OUT_FLASH_V_FILE%
del %REL_PATH%\stm32.bin %REL_PATH%\NoiseSound.bin %REL_PATH%\flash_image_dsp.bin %REL_PATH%\%OUT_SMT_FILE% %REL_PATH%\%DSP_CHECK_SUM_FILE% %REL_PATH%\%CCK_VERSION_FILE%

ECHO ******************** MERGE BIN F I L E ************************************
c:\Python27\python stm32_merge_bin.py %REL_PATH%
ECHO =========== S T A R T   B U I L D I N G  F I N I S H ================

ECHO ******************** MERGE AB1520 BIN F I L E ************************************
del %BT_AB1520_PATH%\%OUT_FILE%
copy %REL_PATH%\%OUT_FILE% %BT_AB1520_PATH%\%OUT_FILE%
c:\Python27\python stm32_merge_bin.py %BT_AB1520_PATH%
ECHO =========== S T A R T   B U I L D I N G  F I N I S H ================

ECHO ******************** COPY RELEASE BIN F I L E ************************************
if exist %OUT_REL_FOLDER% rmdir /s /q %OUT_REL_FOLDER%
xcopy %REL_PATH%\OutputBinFile %OUT_REL_FOLDER% /e /h
copy %REL_PATH%\S3251A.bin %OUT_GSYNC_FOLDER%\S3251A.bin
ECHO =========== COPY RELEASE BIN  F I N I S H ================

ECHO ******************** RAR RELEASE BIN F I L E ************************************
%WINRAR_EXE% a -ep1 -r %WINRAR_FILE% %OUT_REL_FOLDER%
copy %WINRAR_FILE% %OUT_REL_FOLDER%\
del %WINRAR_FILE%
ECHO =========== RAR RELEASE BIN  F I N I S H ================
