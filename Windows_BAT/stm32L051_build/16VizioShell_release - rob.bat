ECHO OFF

SET SHELL_APP_PATH=16VizioShell\App\Exe
SET SHELL_REL_PATH=16VizioShell\Release\Exe
SET SHELL_RELEASE_PATH=16VizioShellRelease
set IAR_BIN_PATH="C:\Program Files (x86)\IAR Systems\Embedded Workbench 7.0\common\bin\iarbuild.exe"
SET VER_FILE=..\AMT\Misc\Inc\Defs.h
SET DEST_PATH=X:\mtk8507\Trunk\portable_spk\MCU_L051

ECHO /********** Reading MCUL051 Version **********/
for /f "tokens=3" %%a in ('findstr main_version %VER_FILE%') do ( set main_ver=%%a ) 
for /f "tokens=3" %%a in ('findstr sub_version %VER_FILE%') do ( set sub_ver=%%a )
set _result=%main_ver:0x=%%sub_ver:0x=%
set _result=%_result: =%
echo MCUL051 Version = %_result%

ECHO /********** S T A R T   B U I L D I N G **********/
ECHO /********** B U I L D   2016 VIZIO SHELL **********/
rmdir %SHELL_RELEASE_PATH% /s /q

%IAR_BIN_PATH% 16VizioShell_app.ewp -clean Application -log info
%IAR_BIN_PATH% 16VizioShell_app.ewp -make Application -log info

%IAR_BIN_PATH% 16VizioShell_loader.ewp -clean Release -log info
%IAR_BIN_PATH% 16VizioShell_loader.ewp -make Release -log info

ECHO /********** S T A R T   B U I L D I N G  F I N I S H **********/
mkdir %SHELL_RELEASE_PATH%
copy %SHELL_APP_PATH%\*.bin %SHELL_RELEASE_PATH%\
copy %SHELL_REL_PATH%\*.bin %SHELL_RELEASE_PATH%\

ECHO /********** C O P Y   B I N A R A Y **********/

mkdir %SHELL_RELEASE_PATH%
copy %SHELL_APP_PATH%\*.bin %SHELL_RELEASE_PATH%\
copy %SHELL_REL_PATH%\*.bin %SHELL_RELEASE_PATH%\

ECHO /********** C O P Y   B I N A R A Y to mtk8507 folder **********/
set DEST_PATH=%DEST_PATH%\%_result%
echo mtk8507_folder = %DEST_PATH%
if exist %DEST_PATH% rmdir /s /q %DEST_PATH%
mkdir %DEST_PATH%
copy %SHELL_APP_PATH%\*.bin %DEST_PATH%\